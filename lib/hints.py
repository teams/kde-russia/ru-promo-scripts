import csv
import re

from typing import List

from lib.markdown_to_plain import unmark

WORDS_REGEX = re.compile(r'(\w+)')

COMMON_TERMS_CASE_SENSITIVE = {
    "Barcode Scanner": "приложение Сканер штрихкодов",
    "Dialer": "номеронабиратель",
    "CSD": "декорации окон на стороне клиента",
    "Escape": "Esc",
    "GPU": "видеокарта / графический ускоритель",
    "Help Center": "Центр справки",
    "Info Center": "Информация о системе",
    "ISO Image Writer": "приложение Запись образов ISO",
    "Itinerary": "приложение Маршруты",
    "KClock": "приложение Часы (KClock)",
    "KCommandBar": "панель команд [в приложениях KDE] (Ctrl+Alt+I)",
    "KFontView": "программа Просмотр шрифтов",
    "KHangMan": "игра Виселица (KHangMan)",
    "Kicker": "классическое меню запуска приложений",
    "Kickoff": "[современное] меню запуска приложений",
    "Kirigami Gallery": "Галерея Kirigami",
    "KInfoCenter": "Информация о системе",
    "Klipper": "диспетчер буфера обмена (Klipper)",
    "KMenuEdit": "Редактор меню KDE",
    "KRecorder": "приложение Диктофон (KRecorder)",
    "KSysGuard": "Системный монитор",
    "KTuberling": "игра Картофельный парень",
    "KWeather": "приложение Погода (KWeather)",
    "OSD": "экранное уведомление/меню",
    "OSDs": "экранные уведомления и меню",
    "Partition Manager": "Диспетчер разделов",
    "Phonebook": "приложение Телефонная книга",
    "Places panel": "панель «Точки входа»",
    "Plasma Camera": "Камера Plasma",
    "Plasma Vaults": "зашифрованные папки Plasma",
    "Print Manager": "Диспетчер печати",
    "Subtitle Composer": "Редактор субтитров KDE",
    "System Monitor": "Системный монитор",
    "System Settings": "Параметры системы",
}

COMMON_TERMS_CASE_INSENSITIVE = {
    "icon": "значок",
    "icons": "значки",
    "location": "расположение (в файловой системе) / местоположение (на карте)",
    "locations": "расположения (в файловой системе) / местоположения (на карте)",
}

COMMON_TERMS_ANY_OCCURRENCE = {
    "applet": "виджет",
    "application launcher": "меню запуска приложений",
    "bugtracker": "система отслеживания ошибок",
    "bug tracker": "система отслеживания ошибок",
    "convergent": "адаптивный",
    "daemon": "служба",
    "directory": "каталог",
    "directories": "каталоги",
    "downstream": "зависимый продукт",
    "emoji": "эмодзи",
    "file manager": "диспетчер файлов",
    "get new [thing]": "диалог загрузки материалов из сети",
    "get new <stuff>": "диалог загрузки материалов из сети",
    "headerbar": "панель заголовка",
    "icon set": "набор значков",
    "icon theme": "набор значков",
    "playlist": "список воспроизведения",
    "screenshot": "снимок экрана",
    "system tray": "системный лоток",
    "task manager": "панель задач",
    "thumbnail": "миниатюра",
    "upstream": "[компонент-]зависимость | исходный репозиторий",
    "venerable": "древний | проверенный временем",
    "window manager": "диспетчер окон",
}

_TECH_DESCRIPTIONS = {}


def load_tech_descriptions():
    with open('tech-descriptions.csv', newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            name, description = row
            _TECH_DESCRIPTIONS[name.lower()] = description


def explain_first_occurrences(text_md: str) -> str:
    if not _TECH_DESCRIPTIONS:
        load_tech_descriptions()

    text_plain = unmark(text_md)
    words = WORDS_REGEX.findall(text_plain)

    described_tech = set()
    for word in words:
        word_lower = word.lower()
        description = _TECH_DESCRIPTIONS.get(word_lower)
        if description is not None and word_lower not in described_tech:
            text_md = text_md.replace(word, f"//{description}// {word}", 1)
            described_tech.add(word_lower)
    return text_md


def recommend_ru_fixes(text_md: str) -> str:
    text_md = text_md.replace("теперь снова", "теперь снова <!-- убрать теперь? -->")
    text_md = text_md.replace("теперь стал", "теперь стал <!-- убрать теперь? -->")
    text_md = text_md.replace("теперь не ", "теперь <!-- заменить на больше? --> не")
    text_md = text_md.replace("количеств", "<!-- число? --> количеств")
    text_md = text_md.replace("имеет ", "имеет <!-- переписать? -->")
    text_md = text_md.replace("имеют ", "имеют <!-- переписать? -->")
    text_md = text_md.replace("корректн ", "<!-- заменить на правильно? --> корректн")
    return text_md


def generate_hints(text: str) -> List[str]:
    hints = []
    for (term, translation) in COMMON_TERMS_CASE_SENSITIVE.items():
        if re.search(r"\b" + re.escape(term) + r"\b", text):
            hints.append((term, translation))
    for (term, translation) in COMMON_TERMS_CASE_INSENSITIVE.items():
        if re.search(r"\b" + re.escape(term) + r"\b", text, re.IGNORECASE):
            hints.append((term, translation))
    text_lower = text.lower()
    for (term, translation) in COMMON_TERMS_ANY_OCCURRENCE.items():
        if term in text_lower:
            hints.append((term, translation))
    return [f"{term} → {translation}" for (term, translation) in sorted(hints)]
