from lingua import Language, LanguageDetectorBuilder


language_detector = LanguageDetectorBuilder.from_languages(Language.ENGLISH, Language.RUSSIAN).build()

def is_russian(line_plain: str) -> bool:
    lang = language_detector.detect_language_of(line_plain)
    return lang == Language.RUSSIAN
