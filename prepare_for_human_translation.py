#!/usr/bin/env python3
import argparse

from pathlib import Path

from lib import utils
from lib.hints import generate_hints, explain_first_occurrences
from lib.translation import should_translate, translate_change_author_md
from lib.utils import split_out_author


def pretranslate_text(text_md: str) -> str:
    input_lines = text_md.splitlines()
    result_lines = []

    for line_md in input_lines:
        if not should_translate(line_md):
            result_lines.append(line_md)
            continue

        line_md, change_author_md = split_out_author(line_md)
        change_author_md = translate_change_author_md(change_author_md)
        result_lines.append(line_md + change_author_md)

    result = '\n'.join(result_lines) + '\n'
    return result


def main():
    parser = argparse.ArgumentParser(prog="prepare_for_human_translation.py")
    parser.add_argument("input_file", type=str, help="Markdown-formatted article to prepare")
    parser.add_argument("--output", type=str,
                        help="Where to save the result (overwrites unless specified)",
                        required=False)
    parser.add_argument("--hints", help="Whether to append glossary hints", action='store_true')
    args = parser.parse_args()

    input_file = Path(args.input_file)
    with input_file.open("r") as f:
        text_md = f.read()

    result = pretranslate_text(text_md)
    result = explain_first_occurrences(result)
    if args.hints:
        hints = generate_hints(result)
        if hints:
            result += '\n' + utils.format_hints(hints) + '\n'

    output_file = Path(args.output) if args.output else input_file
    with output_file.open("w") as f:
        f.write(result)


if __name__ == "__main__":
    main()
