import configparser
from pathlib import Path


CONFIG_PATH = str(Path.home()) + "/.config/kderu"


class Config:
    def __init__(self):
        self._translator_name = "UNSPECIFIED"
        self._translator_link = "https://kde.ru"
        self._is_editor: bool = False
        self._known_translators = []
        p = Path(CONFIG_PATH)
        if p.is_file() or p.is_symlink():
            try:
                with p.open() as f:
                    config = configparser.ConfigParser()
                    config.read_file(f)
                    self._translator_name = config["TRANSLATOR"]["name"]
                    self._translator_link = config["TRANSLATOR"]["link"]
                    if "EDITOR" in config:
                        self._is_editor = config["EDITOR"]["is_editor"] == "True"
                        if self._is_editor:
                            self._known_translators = config["EDITOR"]["known_translators"].split(',')
            except OSError as err:
                print(f"Failed to read the configuration: {err}")
        else:
            self._translator_name = input("Enter your preferred name:   ")
            self._translator_link = input("Enter your preferred blog/profile link:   ")
            editor_ans = input("Are you an editor? [y/N]:   ")
            self._is_editor = editor_ans in ['y', 'д']
            if self._is_editor:
                print("Enter a list of '<translator name> <profile link>', then '.' to stop:")
                cmd = "?"
                while cmd != ".":
                    cmd = input()
                    if cmd != ".":
                        name, link = cmd.rsplit(" ", 1)
                        self._known_translators.append(f"[{name}]({link})")
            self.write()

    def write(self):
        config = configparser.ConfigParser()
        config["TRANSLATOR"] = {"name": self._translator_name, "link": self._translator_link}
        config["EDITOR"] = {
            "is_editor": str(self._is_editor),
            "known_translators": ','.join(self._known_translators)
        }
        p = Path(CONFIG_PATH)
        try:
            with p.open('w') as f:
                config.write(f)
        except OSError as err:
            print(f"Failed to write the configuration: {err}")

    def translator_name(self) -> str:
        return self._translator_name

    def translator_link(self) -> str:
        return self._translator_link

    def translator_attribution(self) -> str:
        my_attrib = f"[{self.translator_name()}]({self.translator_link()})"
        if self._is_editor:
            options = [my_attrib] + self._known_translators
            print("Choose your characters:")
            print('\n'.join([f"{i + 1}) {o}" for (i, o) in enumerate(options)]))
            ids = [i - 1 for i in map(int, input("Indices: ").split(','))]
            return ", ".join([options[i] for i in ids])
        else:
            return my_attrib
