import os
import re
import regex
from datetime import date, timedelta
from functools import reduce
from pathlib import Path
from collections.abc import Iterator
from typing import Optional, List, Tuple, Dict
from urllib.parse import urlparse, parse_qsl, urlunparse

import requests

from lib.config import Config


MONTH_NAMES = [
    "ERR",
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря"
]

READABLE_ENG_MONTH_NAMES = [
    "ERR",
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "jun",
    "jul",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec"
]

CHANGE_AUTHOR_REGEX = regex.compile(r" \([\p{L} ]+, [\p{L}\d. ]+(\[[Ll]ink( \d)?\]\(http.+\)(,| and)? ?)*\):?$")

HEADERS = {
    "Accept-Language" : "en-US,en;q=0.8,ru-RU;q=0.5,ru;q=0.3",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:125.0) Gecko/20100101 Firefox/125.0"
}


def drop_size_limit_query(url_str: str) -> str:
    url = urlparse(url_str)
    query_parts = parse_qsl(url.query)
    query_parts_clean = [f"{name}={value}" for (name, value) in query_parts if name not in ['w']]
    url = url._replace(query='&'.join(query_parts_clean))
    return urlunparse(url)


def extract_url_filename(url_str: str) -> str:
    if url_str.endswith("/"):
        url_str = url_str[:-1]
    if url_str.endswith(".html"):
        url_str = url_str[:-len(".html")]
    url = urlparse(url_str)
    return os.path.basename(url.path)


def extract_domain_name(url_str: str) -> Optional[str]:
    url = urlparse(url_str)
    result = url.hostname
    if result is not None and result.startswith("www."):
        result = result[4:]
    return result


def extract_root(url_str: str) -> str:
    url = urlparse(url_str)
    return f"{url.scheme}://{url.hostname}"


def fetch_url(url: str) -> Optional[str]:
    print(f"Fetching {url} ...")
    response = requests.get(url, headers=HEADERS)
    # Use chardet to detect the encoding.
    if response.encoding == 'ISO-8859-1':
        # No encoding specified explicitly, attempt detection
        response.encoding = response.apparent_encoding
    if not response.ok:
        print(f"Failed to fetch the page: {response.reason}")
        return None
    return response.text


def format_week_range(d: date) -> str:
    end = d + timedelta(days=(6 - d.weekday()))
    start = end - timedelta(days=7)
    if start.month == end.month:
        return f"{start.day}–{end.day} {MONTH_NAMES[start.month]}"
    else:
        return f"{start.day} {MONTH_NAMES[start.month]} – {end.day} {MONTH_NAMES[end.month]}"


def format_disclaimer(d: Optional[date], brief: bool) -> str:
    if d is None:
        return ""
    result = "> " + format_week_range(d)
    if brief:
        result += ", основное"
    return result + " — прим. переводчика\n\n"


def format_authors(author: str, config: Config, url: str, split_authors: bool) -> Tuple[str, str]:
    translation_md = f"*Перевод:* {config.translator_attribution()}  \n"
    translation_md += f"*Источник:* <{url}>  \n"

    if split_authors:
        author_md = f"*Автор:* {author}\n\n"
    else:
        author_md = f"*Автор:* {author}  \n"

    return author_md, translation_md


def format_hints(hints: List[str]) -> str:
    return '\n'.join([f"<!-- 💡: {hint} -->" for hint in hints])


def format_downloadable(downloadable: List[Tuple[str, str]]) -> str:
    result = [f"{i + 1}) [{type_str}] {url}" for (i, (type_str, url)) in enumerate(downloadable)]
    return '\n'.join(result)


def drop_html_ifs(html_str: str):
    return re.sub(r'\[if.*].*!\[endif]', '', html_str)


def strip_lines(text: str) -> str:
    lines = text.split('\n')
    lines = [line.lstrip() for line in lines]
    result = re.sub(r'\n{2,}', '\n\n', '\n'.join(lines))
    return result


def guess_date_yyyymmdd(url: str) -> Optional[date]:
    found = re.search('/(\\d{4})/(\\d{2})/(\\d{2})', url)
    if found:
        year, month, day = map(int, found.groups())
        result = date(year, month, day)
        result += timedelta(days=(6 - result.weekday()))
        return result
    return None


def post_name_from_date(prefix: str, post_date: Optional[date], components="dm") -> str:
    if post_date is None:
        if components == "my":
            date_str = "mon-yyyy"
        else:
            date_str = "DD-MM"
    else:
        if components == "my":
            date_str = f"{READABLE_ENG_MONTH_NAMES[post_date.month]}-{post_date.year}"
        else:
            date_str = post_date.strftime("%d-%m")
    return prefix + '-' + date_str


def find_downloadable_media(md_text: str, article_url_str: str) -> Tuple[List[Tuple[str, str]], str]:
    media_regexes = [
        "\\[!\\[[^\\[\\]]*?]\\(.*?\\)]\\((.+?(?:jpg|jpeg|png|webm|gif)(?:\\?.+?)*)\\)",  # Links around MD images
        "!\\[[^\\[\\]]*?]\\((.+?)\\)",  # MD images
        "<(.*?\\.(?:mp4|webm|mkv)(?:\\?.+?)*)>"  # Direct video links
    ]
    matches = re.findall('|'.join(media_regexes), md_text)
    result = []
    url_replacements = {}
    for (image_full, image, video) in matches:
        type_str = 'F' if image_full else ('I' if image else 'V')
        url = ''.join((image_full, image, video))
        if not url.startswith("data:image"):
            if extract_domain_name(url) is not None:
                good_url = url
            elif url.startswith("/"):
                good_url = extract_root(article_url_str) + url
            else:
                good_url = f"{article_url_str}/{url}"

            good_url = drop_size_limit_query(good_url)
            result.append((type_str, good_url))
            url_replacements[url] = good_url

    new_md_text = multi_replace(md_text, url_replacements)

    return result, new_md_text


def download(downloadable: List[Tuple[str, str]], output_path: str):
    for (i, (type_str, url)) in enumerate(downloadable):
        filename = f"{i}_{extract_url_filename(url)}"
        path = Path(output_path + "/" + filename)
        if path.exists():
            print(f"{path} exists, skipping")
            continue
        print(f"Downloading {url} ...   ", end='', flush=True)
        response = requests.get(url, headers=HEADERS, allow_redirects=True)
        if not response.ok:
            print("[fail]")
            print(f"Reason: {response.reason}", flush=True)
            continue
        print("[done]", flush=True)
        with path.open('wb') as f:
            f.write(response.content)


def store_article(output_dir: str, post_name: str, post_md: str, article_url_str: str):
    Path(output_dir).mkdir(parents=True, exist_ok=True)
    post_dir = output_dir + '/' + post_name
    Path(post_dir).mkdir(exist_ok=True)

    downloadable, post_md = find_downloadable_media(post_md, article_url_str)

    post_text_file = post_dir + '/' + post_name + ".md"
    with open(post_text_file, 'w') as f:
        f.write(post_md)
    print(f"Saved the article to '{post_dir}' ({count_words(post_md)} words)")

    if downloadable:
        print(f"{len(downloadable)} downloadable media files found.")
        print(format_downloadable(downloadable))
        user_response = input("Would you like to download them? [Y/n] ")
        should_download = user_response.lower() in ['y', 'д', '']
        if should_download:
            download(downloadable, post_dir)


def count_words(text) -> int:
    return len(re.findall("[\\w']+", text))


def split_out_author(line_md: str) -> Tuple[str, str]:
    # Do not translate trailing parentheses in a changelog:
    # "This was finally [done](https://example.com) (Sam Smith, Plasma 5.25)"
    change_author_matches = CHANGE_AUTHOR_REGEX.finditer(line_md)
    for match in change_author_matches:
        change_author_md = match.group(0)
        match_start = match.start()
        line_md = line_md[:match_start]
        return line_md, change_author_md
    return line_md, ""


def multi_replace(string: str, replacements: Dict[str, str], ignore_case=False) -> str:
    """
    Given a string and a replacement map, it returns the replaced string.

    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :param bool ignore_case: whether the match should be case insensitive
    :rtype: str

    From https://gist.github.com/bgusach/a967e0587d6e01e889fd1d776c5f3729

    """
    if not replacements:
        return string
    # If case insensitive, we need to normalize the old string so that later a replacement
    # can be found. For instance with {"HEY": "lol"} we should match and find a replacement
    # for "hey", "HEY", "hEy", etc.
    if ignore_case:
        def normalize_old(s: str) -> str:
            return s.lower()

        re_mode = re.IGNORECASE

    else:
        def normalize_old(s: str) -> str:
            return s

        re_mode = 0

    normalized_replacements: Dict[str, str] = {normalize_old(key): val for key, val in replacements.items()}

    # Place longer ones first to keep shorter substrings from matching where the longer ones
    # should take place. For instance, given the replacements {'ab': 'AB', 'abc': 'ABC'}
    # against the string 'hey abc', it should produce 'hey ABC' and not 'hey ABc'
    rep_sorted = sorted(normalized_replacements, key=len, reverse=True)
    rep_escaped = map(re.escape, rep_sorted)

    # Create a big OR regex that matches any of the substrings to replace
    pattern = re.compile("|".join(rep_escaped), re_mode)

    # For each match, look up the new string in the replacements, the key being the normalized
    # old string
    return pattern.sub(lambda match: normalized_replacements[normalize_old(match.group(0))], string)


def compose(*funcs):
    return lambda x: reduce(lambda acc, f: f(acc), funcs, x)


def chunks(l: list, chunk_size: int) -> Iterator[list]:
    for i in range(0, len(l), chunk_size):
        yield l[i:i + chunk_size]
