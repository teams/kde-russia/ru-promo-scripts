#!/usr/bin/env python3
import re
import sys

import requests

from pathlib import Path
from typing import Dict, List, Optional

from lib.utils import chunks
from lib.markdown_to_plain import unmark


BUGZILLA_URL_RE = re.compile(
    "(http(s)?://)?bugs\\.kde\\.org/(?:show_bug\\.cgi\\?id=)?(\\d+)", flags=re.ASCII)
REST_URL_BASE = "https://bugs.kde.org/rest/bug?id="
MAX_BUGS_PER_REQUEST = 50


class Bug:
    def __init__(self, bug_id: int, title: str, cc_count: int):
        self.bug_id = bug_id
        self.title = title
        self.cc_count = cc_count
        self.translations = []

    def add_translations(self, translations: List[str]):
        self.translations += translations

    def __str__(self):
        s = f"https://bugs.kde.org/{self.bug_id} ({self.title}) : {self.cc_count}"
        if self.translations:
            s += "\n* " + "\n* ".join(self.translations)
        return s

    def __repr__(self):
        return f"Bug({self.bug_id}, \"{self.title}\", CC={self.cc_count})"


def extract_bug_ids(text: str) -> List[int]:
    result = []
    for match in BUGZILLA_URL_RE.finditer(text):
        bug_id_str = match.group(3)
        try:
            bug_id = int(bug_id_str)
            result.append(bug_id)
        except ValueError:
            print(f"Invalid bug ID: {bug_id_str}", file=sys.stderr)
            continue
    return result


def fetch_bug_info(ids: List[int], translations_dir_path: Optional[Path]) -> Dict[int, Bug]:
    bugs = {}

    id_chunks = chunks(ids, MAX_BUGS_PER_REQUEST)
    for chunk in id_chunks:
        rest_url = REST_URL_BASE + ",".join(map(str, chunk))
        print(f"Requesting {rest_url}", file=sys.stderr)
        rest_json = requests.get(rest_url).json()
        bugs_json = rest_json["bugs"]

        for bug in bugs_json:
            i = bug.get("id")
            cc = bug.get("cc")
            cc_count = len(cc)
            title = bug.get("summary")
            bugs[i] = Bug(i, title, cc_count)

        for i in chunk:
            if i not in bugs:
                bugs[i] = Bug(i, "Not found?", 0)

    if translations_dir_path:
        print(f"Looking for translations in {translations_dir_path}", file=sys.stderr)
        translations = find_translations_with_substrings(translations_dir_path, [str(i) for i in bugs.keys()])
        for i_str, translations in translations.items():
            bugs[int(i_str)].add_translations(translations)

    return bugs


def find_translations_with_substrings(dir_path: Path, substrings: List[str]) -> Dict[str, List[str]]:
    result = {}
    for file_path in sorted(dir_path.glob("**/*.md")):
        with file_path.open('r') as f:
            lines = f.readlines()
            for line_md in lines:
                line_plain = unmark(line_md)
                for substring in substrings:
                    if substring in line_md:
                        result.setdefault(substring, []).append(line_plain)
    return result


def main():
    filename = sys.argv[1]
    translations_dir_path = Path(sys.argv[2])
    if filename.startswith("http"):
        print(f"Downloading {filename}", file=sys.stderr)
        file_content = requests.get(filename).text
    else:
        print(f"Reading {filename}", file=sys.stderr)
        with open(filename, 'r') as f:
            file_content = f.read()

    bug_ids = extract_bug_ids(file_content)
    bug_ids = list(set(bug_ids))
    print(f"Found {len(bug_ids)} bugs", file=sys.stderr)

    bugs: Dict[int, Bug] = fetch_bug_info(bug_ids, translations_dir_path)

    sorted_ids = sorted(bug_ids, key=lambda i: int(bool(bugs[i].translations)) * 1000 + bugs[i].cc_count, reverse=True)
    sorted_bugs = [str(bugs[i]) for i in sorted_ids]

    print("\n\n".join(sorted_bugs))


main()
