import logging
import re

from typing import Optional


CYRILLIC_WORDS_REGEX = re.compile(r'[а-яёА-ЯЁ]+')

_SAFE_DICT = {}
_UNSAFE_DICT = {}


logger = logging.getLogger(__name__)


def uneyo(s: str):
    return s.replace('Ё', 'Е').replace('ё', 'е')


def add_word_internal(word: str, d: dict):
    # The word can only start with a lowercase letter: _киёв, but Киев.
    hasUnderscore = word.startswith('_')
    if hasUnderscore:
        word = word[1:]

    key = uneyo(word)
    d[key] = word

    if word == word.lower() and not hasUnderscore:
        d[key.capitalize()] = word.capitalize()


def add_word(word: str, d: dict):
    # Comment
    if '#' in word:
        word = word.split('#', 1)[0].strip()

    if '(' in word:
        # аистёнк(а|е|ом|у)
        parts = re.split("[(|)]", word)
        for p in parts[1:-1]:
            add_word_internal(parts[0] + p, d)
    else:
        add_word_internal(word, d)


def load_dict_file(dict_filename: str, d: dict):
    with open (dict_filename, 'r') as f:
        dict_lines = f.read().splitlines()
    for line in dict_lines:
        add_word(line, d)


def get_safe_replacement(word: str) -> Optional[str]:
    if not _SAFE_DICT:
        load_dict_file("yo-dicts/safe.txt", _SAFE_DICT)
    return _SAFE_DICT.get(word)


def get_unsafe_replacement(word: str) -> Optional[str]:
    if not _UNSAFE_DICT:
        load_dict_file("yo-dicts/not_safe.txt", _UNSAFE_DICT)
    return _UNSAFE_DICT.get(word)


def eyo_md(text_md: str) -> str:
    word_matches = CYRILLIC_WORDS_REGEX.finditer(text_md)
    result = ""
    prev_index = 0
    for match in word_matches:
        result += text_md[prev_index:match.start()]
        prev_index = match.end()

        word = match.group(0)
        safe_replacement = get_safe_replacement(word)
        if safe_replacement is not None:
            result += safe_replacement
            logger.info(f"Ё: safe replacement {word} → {safe_replacement}")
        else:
            unsafe_replacement = get_unsafe_replacement(word)
            if unsafe_replacement is not None:
                result += f"{word} <!-- {unsafe_replacement}? -->"
                logger.info(f"Ё: possible unsafe replacement {word} → {unsafe_replacement}")
            else:
                result += word
    result += text_md[prev_index:len(text_md)]
    return result
