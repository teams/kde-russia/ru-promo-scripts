from typing import Optional, Tuple

from lib import utils
from lib.html_selector import Selector


def get_website_specifics(url_str: str) -> Optional[Tuple[Selector, str, bool, bool, str]]:
    """
    This function describes website-specific options that influence article processing.
    For a website, the following specifics are defined:
    * selector: CSS selectors for the article title and content.
    * post_name: directory and file names to use for the article, potentially also used
                 in URL when publishing the translation.
    * add_disclaimer: whether to add a "translator's note" specifying when the article
                      was originally posted, to clarify that it might be not up-to-date.
    * split_authors: whether to specify the author before article content, useful when
                     the author is important and not obvious to the reader.
    * author_attribution: author's name and bio link (if available).
    Please keep the websites grouped and sorted alphabetically!
    :param url_str: URL to use for parameter detection.
    :return: website-specific options, as described above.
    """
    domain = utils.extract_domain_name(url_str)
    post_date = utils.guess_date_yyyymmdd(url_str)
    url_filename = utils.extract_url_filename(url_str)

    # ### CONTRIBUTOR BLOGS ###
    if domain == "blog.blackquill.cc":
        selector: Selector = [
            # Title
            "#title",
            # Content
            ".e-content"
        ]
        post_name = "blackquill-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "Janet Blackquill"
    elif domain == "blog.broulik.de":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = "kbroulik-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "Kai Uwe Broulik"
    elif domain == "blog.david-redondo.de":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".post-content"
        ]
        post_name = "davidre-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[David Redondo](https://blog.david-redondo.de/about/)"
    elif domain == "blog.davidedmundson.co.uk":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = "d_ed-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "David Edmundson"
    elif domain == "carlschwan.eu":
        selector: Selector = [
            # Title
            ".main-article .article-title",
            # Content
            "section.article-content"
        ]
        post_name = "carl-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Carl Schwan](https://carlschwan.eu/about/)"
    elif domain == "claudiocambra.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = "cambra-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Claudio Cambra](https://claudiocambra.com/)"
    elif domain == "cmollekopf.wordpress.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = "cmollekopf-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Christian Mollekopf](https://cmollekopf.wordpress.com/author/cmollekopf/)"
    elif domain == "frinring.wordpress.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = "frinring-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Friedrich Kossebau](https://frinring.wordpress.com)"
    elif domain == "jriddell.org":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = "jriddell-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Jonathan Riddell](https://jriddell.org/about-me/)"
    elif domain == "jucato.wordpress.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = "jucato-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Juan Carlos G. Torres](https://jucato.wordpress.com/about/)"
    elif domain == "nicolasfella.de":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".content"
        ]
        post_name = "nicofe-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Nicolas Fella](https://nicolasfella.de/about/)"
    elif domain == "notmart.org":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = "notmart-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "Marco Martin"
    elif domain == "pointieststick.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = utils.post_name_from_date("nedelya", post_date)
        add_disclaimer = True
        split_authors = False
        author_attribution = "[Nate Graham](https://pointieststick.com/author/pointieststick/)"
    elif domain == "proli.net":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".post-content"
        ]
        post_name = "aleix-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Aleix Pol Gonzalez](https://www.proli.net/about/)"
    elif domain == "psifidotos.blogspot.com":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".post-body"
        ]
        post_name = url_filename
        add_disclaimer = False
        split_authors = False
        author_attribution = "[Michail Vourlakos](https://psifidotos.blogspot.com/p/my-profile.html)"
    elif domain == "quantumproductions.info":
        selector: Selector = [
            # Title
            ".page-title",
            # Content
            ".text-content"
        ]
        post_name = "ahiemstra-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Arjen Hiemstra](https://invent.kde.org/ahiemstra)"
    elif domain == "quickfix.es":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = "paulbrown-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Paul Brown](https://quickfix.es/about/)"
    elif domain == "simonredman.wordpress.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = "simonredman-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Simon Redman](https://simonredman.wordpress.com)"
    elif domain == "sirgienkogsoc2019.blogspot.com":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".post-body"
        ]
        post_name = "sirgienko-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Никита Сиргиенко](https://www.blogger.com/profile/09245922798753936049)"
    elif domain == "stikonas.eu":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = "stikonas-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Andrius Štikonas](https://stikonas.eu/wordpress)"
    elif domain == "subdiff.org":
        selector: Selector = [
            # Title
            ".post-content h1.title",
            # Content
            ".post-content article.content"
        ]
        post_name = "subdiff-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Roman Gilg](https://subdiff.org/about)"
    elif domain == "tcanabrava.github.io":
        selector: Selector = [
            # Title
            ".mx-auto .lh-condensed",
            # Content
            ".article"
        ]
        post_name = "tcanabrava-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Tomaz Canabrava]https://tcanabrava.github.io)"
    elif domain == "blog.vladzahorodnii.com":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            (".entry-content", [".sharedaddy"])
        ]
        post_name = "vlad-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Влад Загородний](https://blog.vladzahorodnii.com)"
    elif domain == "volkerkrause.eu":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".post-content"
        ]
        post_name = "volker-" + url_filename[:-len(".html")]
        add_disclaimer = False
        split_authors = True
        author_attribution = "Volker Krause"
    elif domain == "write.as":
        selector: Selector = [
            # Title
            "#post-body time",
            # Content
            ".e-content"
        ]
        post_name = "niccolo-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Niccolo Venerandi](https://niccolo.venerandi.com)"
    elif domain == "zamundaaa.github.io":
        selector: Selector = [
            # Title
            ".post-title",
            # Content
            ".post-content"
        ]
        post_name = "zamundaaa-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "[Xaver Hugl](https://zamundaaa.github.io/about/)"
    # ### PROJECT BLOGS ###
    elif domain == "blog.neon.kde.org":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".entry-content"
        ]
        post_name = url_filename
        if "neon" not in post_name:
            post_name = "neon-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "Сопровождающие KDE Neon"
    elif domain == "digikam.org":
        selector: Selector = [
            # Title
            "article h1",
            # Content
            ("article", ["h1", "time", ".author", ".post-meta", ".discourse-comments"])
        ]
        post_name = url_filename
        if "digikam" not in post_name:
            post_name = "digikam-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "Разработчики digiKam"
    elif domain == "kate-editor.org":
        selector: Selector = [
            # Title
            "article h1",
            # Content
            ("article", ["h1", ".meta"])
        ]
        post_name = url_filename
        if "kate" not in post_name:
            post_name = "kate-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "Разработчики Kate"
    elif domain == "kdenlive.org":
        selector: Selector = [
            # Title
            "article .entry-title",
            # Content
            ".entry-content"
        ]
        post_name = url_filename
        if "kdenlive" not in post_name:
            post_name = "kdenlive-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "Команда Kdenlive"
    elif domain == "krita.org":
        selector: Selector = [
            # Title
            ".main-title",
            # Content
            (".post", [".main-title", ".ssba"])
        ]
        post_name = url_filename
        if "krita" not in post_name:
            post_name = "krita-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "Команда Krita"
    elif domain == "kubuntu.org":
        selector: Selector = [
            # Title
            ".title",
            # Content
            ".skepost"
        ]
        post_name = url_filename
        if "kubuntu" not in post_name:
            post_name = "kubuntu-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "Сопровождающие Kubuntu"
    elif domain == "plasma-mobile.org":
        selector: Selector = [
            # Title
            ".post-content h1",
            # Content
            (".post-content", ["h1", ".meta"])
        ]
        post_name = utils.post_name_from_date("plasma-mobile", post_date, components="my")
        add_disclaimer = False
        split_authors = False
        author_attribution = "Команда Plasma Mobile"
    # ### KDE NEWS WEBSITES ###
    elif domain == "blogs.kde.org":
        selector: Selector = [
            # Title
            ".node-content h2",
            # Content
            ".node-content .content"
        ]
        post_name = "dot-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "Редакторы KDE Dot News"
    elif domain == "dot.kde.org":
        selector: Selector = [
            # Title
            ".page-header",
            # Content
            (".region-content", [".links"])
        ]
        post_name = "dot-" + url_filename
        add_disclaimer = False
        split_authors = False
        author_attribution = "Редакторы KDE Dot News"
    # ### EXTERNAL ###
    elif domain == "kdab.com":
        selector: Selector = [
            # Title
            ".wpcontent h1",
            # Content
            (".wpcontent .col-sm-12", [
                "h1", ".the_meta", ".the_tags", ".the_comments",
                ".panel", ".addtoany_share_save_container"
            ])
        ]
        post_name = url_filename
        if "kdab" not in post_name:
            post_name = "kdab-" + post_name
        add_disclaimer = False
        split_authors = True
        author_attribution = "**TODO**"
    elif domain == "nxos.org":
        selector: Selector = [
            # Title
            ".entry-title",
            # Content
            ".post-content"
        ]
        post_name = "nxos-" + url_filename
        add_disclaimer = False
        split_authors = True
        author_attribution = "Команда Nitrux"
    elif domain == "qt.io":
        selector: Selector = [
            # Title
            "article h1",
            # Content
            "#hs_cos_wrapper_post_body"
        ]
        post_name = url_filename
        if "qt" not in post_name:
            post_name = "qt-" + post_name
        add_disclaimer = False
        split_authors = False
        author_attribution = "**TODO**"
    elif domain == "vk.com":
        selector: Selector = [
            # Title
            ".article h1",
            # Content
            (".article", ["h1", ".article__info_line"])
        ]
        # Drop the leading '@'
        post_name = url_filename[1:]
        add_disclaimer = False
        split_authors = False
        author_attribution = "**REMOVE THESE**"
    # ### UNKNOWN ###
    else:
        return None

    assert not (None in (selector, post_name, add_disclaimer, split_authors, author_attribution))
    return selector, post_name, add_disclaimer, split_authors, author_attribution
