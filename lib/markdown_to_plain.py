from io import StringIO

import markdown


def unmark_element(element, stream=None) -> str:
    if stream is None:
        stream = StringIO()
    if element.text:
        stream.write(element.text)
    for sub in element:
        unmark_element(sub, stream)
    if element.tail:
        stream.write(element.tail)
    return stream.getvalue()


markdown.Markdown.output_formats["plain"] = unmark_element # type: ignore
# noinspection PyTypeChecker
__md_plain = markdown.Markdown(output_format="plain") # type: ignore
__md_plain.stripTopLevelTags = False


def unmark(text: str) -> str:
    return __md_plain.convert(text)
