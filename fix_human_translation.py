#!/usr/bin/env python3
import argparse

from pathlib import Path

from lib.edits import edit
from lib.eyo import eyo_md
from lib.hints import recommend_ru_fixes


def main():
    parser = argparse.ArgumentParser(prog="fix_human_translation.py")
    parser.add_argument("input_file", type=str, help="Markdown-formatted translation to fix")
    parser.add_argument("--output", type=str,
                        help="Where to save the result (overwrites unless specified)",
                        required=False)
    args = parser.parse_args()

    input_file = Path(args.input_file)
    with input_file.open("r") as f:
        text_md = f.read()

    result = edit(text_md)
    result = eyo_md(result)
    result = recommend_ru_fixes(result)

    output_file = Path(args.output) if args.output else input_file
    with output_file.open("w") as f:
        f.write(result)


if __name__ == "__main__":
    main()
