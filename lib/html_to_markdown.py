from markdownify import MarkdownConverter


class ImageBlockConverter(MarkdownConverter):
    def convert_a(self, el, text, convert_as_inline):
        if 'img' in [c.name for c in el.children]:
            suffix = '\n\n'
        else:
            suffix = ''
        return super().convert_a(el, text, convert_as_inline) + suffix

    def convert_img(self, el, text, convert_as_inline):
        return super().convert_img(el, text, convert_as_inline) + '\n\n'


# Create shorthand method for conversion
def html_to_md(text_html: str, **options) -> str:
    return ImageBlockConverter(**options).convert(text_html)

def html_to_md_default(text_html: str) -> str:
    return html_to_md(text_html, heading_style='ATX')
