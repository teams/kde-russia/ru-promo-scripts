import htmlmin
from bs4 import BeautifulSoup
from typing import List, Tuple, Union


Selector = List[Union[str, Tuple[str, List[str]]]]


def replace_node_with_url(soup, node, url):
    video_p_tag = soup.new_tag("p")
    video_a_tag = soup.new_tag("a")
    video_a_tag["href"] = url
    video_a_tag.string = url
    video_p_tag.append(video_a_tag)
    node.replace_with(video_p_tag)


def videos_to_urls(soup):
    video_nodes = soup.find_all("video")
    for n in video_nodes:
        if "src" in n.attrs:
            video_url = n["src"]
        elif n.source is not None:
            video_url = n.source["src"]
        else:
            print("WARNING: video node without src! Skipping.")
            continue
        replace_node_with_url(soup, n, video_url)

    iframe_nodes = soup.find_all("iframe")
    for n in iframe_nodes:
        if "src" in n.attrs:
            iframe_url = n["src"]
            for website in ["youtube.com", "youtube-nocookie.com", "conf.tube"]:
                if website in iframe_url:
                    replace_node_with_url(soup, n, iframe_url)
                    break


def un_lazy_load_images(soup):
    img_nodes = soup.find_all("img")
    for n in img_nodes:
        css_classes = n.attrs.get("class")
        if n.attrs.get("loading") == "lazy" or (css_classes and "lazy" in css_classes):
            real_url = n.attrs.get("data-src")
            if real_url is not None:
                n["src"] = real_url


def maximize_srcset(soup):
    img_nodes = soup.find_all("img")
    for n in img_nodes:
        srcset = n.attrs.get("srcset")
        if srcset is None:
            continue
        srcset_parts = [x.strip().split(" ") for x in srcset.split(",")]
        best_url, max_size = None, 0
        for url, spec in srcset_parts:
            size = float(spec[:-1])
            if size > max_size:
                max_size = size
                best_url = url
        if best_url is not None:
            n["src"] = best_url


def img_title_to_p(soup):
    img_nodes = soup.find_all("img")
    for n in img_nodes:
        title = n.attrs.get("title")
        if title:
            n.attrs.pop("title")
            title_p_tag = soup.new_tag("p")
            title_i_tag = soup.new_tag("i")
            title_i_tag.string = title
            title_p_tag.append(title_i_tag)
            n.insert_after(title_p_tag)


def un_redirect_links(soup):
    link_nodes = soup.find_all("a")
    for n in link_nodes:
        n_title = n.attrs.get("title")
        if n_title is not None and n_title.startswith("http"):
            # Appears that the actual link is in a tooltip
            n["href"] = n_title
            n.attrs.pop("title")


def remove_image_self_links(soup):
    img_nodes = soup.select("a > img")
    for img_n in img_nodes:
        a_n = img_n.parent
        a_url = a_n.attrs.get("href")
        img_src = img_n.attrs.get("src")
        # Having images stored on Imgur also link to Imgur is a common pattern
        if a_url == img_src or ("imgur.com" in a_url and "imgur.com" in img_src):
            # The <a> tag can have multiple children => keep them all
            a_n.unwrap()


def drop_empty_headings(soup):
    heading_nodes = soup.find_all([f"h{i}" for i in range(1, 7)])
    for n in heading_nodes:
        if not n.get_text(strip=True):
            n.decompose()


def figure_captions_to_paragraphs(soup):
    figcaption_nodes = soup.find_all("figcaption")
    for n in figcaption_nodes:
        caption_text = n.get_text(strip=True)
        caption_p_tag = soup.new_tag("p")
        caption_i_tag = soup.new_tag("i")
        caption_i_tag.string = caption_text
        caption_p_tag.append(caption_i_tag)
        n.replace_with(caption_p_tag)


def select_html(html_doc: str, selector: Selector) -> List[str]:
    html_doc = htmlmin.minify(html_doc)
    soup = BeautifulSoup(html_doc, 'html5lib')

    videos_to_urls(soup)
    drop_empty_headings(soup)
    un_lazy_load_images(soup)
    maximize_srcset(soup)
    img_title_to_p(soup)
    un_redirect_links(soup)
    figure_captions_to_paragraphs(soup)
    remove_image_self_links(soup)

    result = []
    for s in selector:
        if isinstance(s, str):
            include = s
            excludes = []
        else:
            include, excludes = s
        nodes = soup.select(include)
        selection = ""
        for node in nodes:
            for exclude in excludes:
                exclude_nodes = node.select(exclude)
                for n in exclude_nodes:
                    n.decompose()
            selection += str(node)
        result.append(selection)
    return result
