import re

from lib.edits import strip_repeating_whitespace
from lib.language_detection import is_russian
from lib.markdown_to_plain import unmark
from lib.utils import compose, multi_replace


TRANSLATABLE_NAMES = {
    "Alexey Andreyev": "Алексей Андреев",
    "Alexander Kernozhitsky": "Александр Керножицкий",
    "Alexander Potashev": "Александр Поташев",
    "Alexander Saoutkin": "Александр Сауткин",
    "Alexander Volkov": "Александр Волков",
    "Aleksei Nikiforov": "Алексей Никифоров",
    "Andrey Butirsky": "Андрей Бутырский",
    "Andrew Butirsky": "Андрей Бутырский",
    "Andrew Shark": "Андрей Акулов",
    "Artem Grinev": "Артём Гринёв",
    "Egor Ignatov": "Егор Игнатов",
    "Eugene Popov": "Евгений Попов",
    "Evgeniy Harchenko": "Евгений Харченко",
    "Gleb Popov": "Глеб Попов",
    "Iaroslav Sheveliuk": "Ярослав Шевелюк",
    "Igor Poboiko": "Игорь Побойко",
    "Ilia Kats": "Илья Кац",
    "Ilya Bizyaev": "Илья Бизяев",
    "Ilya Pominov": "Илья Поминов",
    "Ivan Tkachenko": "Иван Ткаченко",
    "Konstantin Kharlamov": "Константин Харламов",
    "Marat Radchenko": "Марат Радченко",
    "Maxim Leshchenko": "Максим Лещенко",
    "Mikhail Zolotukhin": "Михаил Золотухин",
    "Nikita Karpei": "Никита Карпей",
    "Nikolai Krasheninnikov": "Николай Крашенинников",
    "Oleg Solovyov": "Олег Соловьёв",
    "Serg Podtynnyi": "Сергей Подтынный",
    "Slava Aseev": "Слава Асеев",
    "Valerii Vtorygin": "Валерий Вторыгин",
    "Vlad Zagorodniy": "Влад Загородний",  # old spelling
    "Vlad Zahorodnii": "Влад Загородний",
    "Vyacheslav Mayorov": "Вячеслав Майоров",
    "Yaroslav Sidlovsky": "Ярослав Сидловский",
    "Yuri Chornoivan": "Юрий Чорноиван",
}

TRANSLATABLE_HEADINGS = {
    "This week in KDE": "На этой неделе в KDE",
    "New Features": "Новые возможности",
    "Other new features": "Другие новые возможности",
    "Other Significant Bugfixes": "Другие заметные исправления",
    "Other User Interface Improvements": "Другие улучшения пользовательского интерфейса",
    "Automation & Systematization": "Автоматизация и систематизация",
    "Bugfixes": "Исправления ошибок",
    "15-Minute Bugs Resolved": "Исправленные «15-минутные ошибки»",
    "15-Minute Bugs": "15-минутные ошибки",
    "Significant Bugfixes": "Заметные исправления",
    "Performance Improvements": "Улучшения производительности",
    "Performance & Technical": "Производительность и технические улучшения",
    "Bugfixes & Performance Improvements": "Исправления ошибок и улучшения производительности",
    "Bug Fixes": "Исправления ошибок",
    "User Interface Improvements": "Улучшения пользовательского интерфейса",
    "UI Improvements": "Улучшения пользовательского интерфейса",
    "Changes not in KDE that affect KDE": "Изменения вне KDE, затрагивающие KDE",
    "Web Presence": "Сетевое присутствие",
    "How You Can Help": "Как вы можете помочь",
}

TRANSLATABLE_FRAGMENTS = {
    "(This is a curated list of e.g. HI and VHI priority bugs, Wayland showstoppers, major regressions, etc.)":\
        "> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)",
    "Other bug-related information of interest:": "Другие сведения об ошибках, которые могут вас заинтересовать:",
    "Other bug information of note:": "Другие сведения об ошибках:",
    "[Current list of bugs]": "[Текущий список ошибок]",
    "[Full list of bugs]": "[Полный список ошибок]",
    "[General info]": "[Общие сведения]",
    "[Open issues:": "[Отслеживаемые проблемы:",
    "(same as last week)": "(как и на прошлой неделе)",
    "(same as two weeks ago)": "(как и две недели назад)",
}

# 0: 21 ошибка
# 1: 2 ошибки
# 2: 5 ошибок
# 3: одна ошибка
TRANSLATABLE_PLURALS = {
    "(\\d+) Very high priority Plasma bugs": [
        "Осталась \\1 ошибка в Plasma с очень высоким приоритетом",
        "Осталось \\1 ошибки в Plasma с очень высоким приоритетом",
        "Осталось \\1 ошибок в Plasma с очень высоким приоритетом",
        "Осталась одна ошибка в Plasma с очень высоким приоритетом",
    ],
    "(\\d+) 15-minute Plasma bugs": [
        "Известных 15-минутных ошибок в Plasma осталось \\1",
        "Известных 15-минутных ошибок в Plasma осталось \\1",
        "Известных 15-минутных ошибок в Plasma осталось \\1",
        "Осталась одна известная 15-минутная ошибка в Plasma",
    ],
    "(\\d+) KDE bugs of all kinds fixed over (?:the )?last week": [
        "Суммарно \\1 ошибка была исправлена на этой неделе во всём программном обеспечении KDE",
        "Суммарно \\1 ошибки были исправлены на этой неделе во всём программном обеспечении KDE",
        "Суммарно \\1 ошибок было исправлено на этой неделе во всём программном обеспечении KDE",
        "Суммарно одна ошибка была исправлена на этой неделе во всём программном обеспечении KDE",
    ],
    "(\\d+) KDE bugs of all kinds fixed over (?:the past|these) two weeks": [
        "Суммарно \\1 ошибка была исправлена за две недели во всём программном обеспечении KDE",
        "Суммарно \\1 ошибки были исправлены за две недели во всём программном обеспечении KDE",
        "Суммарно \\1 ошибок было исправлено за две недели во всём программном обеспечении KDE",
        "Суммарно одна ошибка была исправлена за две недели во всём программном обеспечении KDE",
    ],
    "\\((?:up|down) from (\\d+) last week\\)": [
        "(на прошлой неделе была \\1)",
        "(на прошлой неделе было \\1)",
        "(на прошлой неделе было \\1)",
        "(на прошлой неделе была одна)",
    ],
    "\\((?:up|down) from (\\d+) two weeks ago\\)": [
        "(две недели назад была \\1)",
        "(две недели назад было \\1)",
        "(две недели назад было \\1)",
        "(две недели назад была одна)",
    ],
    "\\((\\d+) more than last week\\)": [
        "(на \\1 больше, чем на прошлой неделе)",
        "(на \\1 больше, чем на прошлой неделе)",
        "(на \\1 больше, чем на прошлой неделе)",
        "(на одну больше, чем на прошлой неделе)",
    ],
    "\\((\\d+) fewer than last week\\)": [
        "(на \\1 меньше, чем на прошлой неделе)",
        "(на \\1 меньше, чем на прошлой неделе)",
        "(на \\1 меньше, чем на прошлой неделе)",
        "(на одну меньше, чем на прошлой неделе)",
    ],
}

LINK_MAYBE_NUMBERED_RE = re.compile(r"\[[Ll]ink( \d)?\]")


def translate_names(text: str):
    return multi_replace(text, TRANSLATABLE_NAMES, True)


def translate_common_headings(text: str):
    md_dict = {}
    prefix = "# "
    for k, v in TRANSLATABLE_HEADINGS.items():
        md_dict[prefix + k] = prefix + TRANSLATABLE_HEADINGS[k]
    return multi_replace(text, md_dict, True)


def translate_common_fragments(text: str):
    return multi_replace(text, TRANSLATABLE_FRAGMENTS)


def basic_translate(text_md: str) -> str:
    f = compose(translate_names, translate_common_headings,
                translate_common_fragments, translate_plurals)
    return f(text_md)


def translate_change_author_md(change_author_md: str) -> str:
    result = change_author_md\
        .replace(", and ", " и ")\
        .replace(" and ", " и ")\
        .replace("Someone going by the pseudonym", "Кто-то под псевдонимом")\
        .replace("Someone who wishes to remain anonymous", "Кто-то, кто предпочёл остаться анонимным")
    result = LINK_MAYBE_NUMBERED_RE.sub(r"[Ссылка\1]", result, count=1)
    result = LINK_MAYBE_NUMBERED_RE.sub(r"[ссылка\1]", result)

    return result


def should_translate(line_md: str) -> bool:
    if not line_md.strip():
        return False  # Blank line
    if line_md.startswith("!"):
        return False  # Image
    if line_md.startswith("<!--") and line_md.endswith("-->"):
        return False  # Full-line comment

    line_plain = unmark(line_md)
    if is_russian(line_plain):
        return False

    return True


def apply_alignment_fixes(source_md: str, translation_md: str) -> str:
    if not source_md.endswith('.'):
        translation_md = translation_md.rstrip('.')
    translation_md = strip_repeating_whitespace(translation_md)
    while source_md.endswith("  ") and not translation_md.endswith("  "):
        translation_md += " "
    return translation_md


def russian_plural_form_for_number(n: int) -> int:
    # nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
    if n == 1:
        return 3
    if n % 10 == 1 and n % 100 != 11:
        return 0
    if 2 <= n % 10 <= 4 and (n % 100 < 10 or n % 100 >= 20):
        return 1
    return 2


def translate_plurals(text: str) -> str:
    for pattern, translations in TRANSLATABLE_PLURALS.items():
        match = re.search(pattern, text)
        while match:
            source = match.group(0)
            n = int(match.group(1))
            form = russian_plural_form_for_number(n)
            translation = translations[form].replace("\\1", str(n))
            text = text.replace(source, translation)
            match = re.search(pattern, text)

    return text
