# ru-promo-scripts

## article_to_md

This script is used by the Russian Promo team to convert KDE-related blog posts and news to human-readable Markdown for simplified translation and automated edits.

Dependency installation: `pip3 install -r requirements.txt`

Usage: `./article_to_md.py -h`

On the first launch, you will be prompted to provide your (or team's) contact details to be specified in translations. They are then stored in `~/.config/kderu`.
