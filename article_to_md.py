#!/usr/bin/env python3
import argparse

from lib.html_selector import select_html, Selector
from lib.html_to_markdown import html_to_md_default

from lib import utils
from lib.config import Config
from lib.edits import edit
from lib.translation import basic_translate
from lib.websites import get_website_specifics


def article_to_md(html: str, url: str, add_disclaimer: bool, split_authors: bool,
               selector: Selector, author_attribution: str, config: Config) -> str:
    post_date = utils.guess_date_yyyymmdd(url)

    selection = select_html(html, selector)

    def to_clean_md(x):
        m = html_to_md_default(utils.drop_html_ifs(x))
        if not m.endswith("\n"):
            m += "\n"
        return m

    converted = list(map(to_clean_md, selection))
    title_md, content_md = converted

    if not title_md.startswith("#"):
        title_md = "# " + title_md
    if not title_md.endswith("\n\n"):
        title_md += "\n"
    if not content_md.endswith("\n\n"):
        content_md += "\n"

    if add_disclaimer:
        disclaimer_md = utils.format_disclaimer(post_date, brief=True)
    else:
        disclaimer_md = ""
    authors_md, translation_md = utils.format_authors(author_attribution, config, url, split_authors)

    if split_authors:
        combined = title_md + authors_md + disclaimer_md + content_md + translation_md
    else:
        combined = title_md + disclaimer_md + content_md + authors_md + translation_md
    result = utils.strip_lines(combined)

    return result


def main():
    parser = argparse.ArgumentParser(prog="article_to_md.py")
    parser.add_argument("url", help="Article to download and convert", type=str)
    parser.add_argument("output_dir", help="Directory for saved articles", type=str,
                        nargs='?', default=".")
    args = parser.parse_args()

    config = Config()

    url_str = args.url
    output_dir = args.output_dir

    response_html = utils.fetch_url(url_str)
    if response_html is None:
        print("Failed to download the web page.")
        return

    specifics = get_website_specifics(url_str)
    if specifics is None:
        print("Error: Unknown website, please describe first (websites.py).")
        return
    selector, post_name, add_disclaimer, split_authors, author_attribution = specifics

    result = article_to_md(response_html, url_str, add_disclaimer, split_authors,
                        selector, author_attribution, config)
    result = edit(result)
    result = basic_translate(result)

    utils.store_article(output_dir, post_name, result, url_str)


main()
