import markdown

def md_to_html(text_md: str) -> str:
    return markdown.markdown(text_md)
