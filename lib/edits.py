import re

from lib.utils import compose, multi_replace


TRIVIAL_FIXES = {
    " - ": " — ",
    " -- ": " — ",
    " --- ": " — ",
    "...": "…",
    " ,": ",",
    " .": ".",
    " !": "!",
    " )": ")",
    " ”": "”",
    " »": "»",
    " :": ":",
}

AUTOMATIC_EDITS = {
    "me: Nate Graham": "Nate Graham",
    "QQC2": "Qt Quick Controls 2",
    "PlaMo": "Plasma Mobile",
    "GTK4": "GTK 4",
}

REPEATING_WHITESPACE_RE = re.compile(r'\s+')


def apply_trivial_fixes(text: str) -> str:
    return multi_replace(text, TRIVIAL_FIXES)


def strip_repeating_whitespace(text: str) -> str:
    return REPEATING_WHITESPACE_RE.sub(" ", text)


def apply_automatic_edits(text: str) -> str:
    return multi_replace(text, AUTOMATIC_EDITS)


def convert_quotation_marks(text: str) -> str:
    result = re.sub(r'"(.{,100}?)"', r'«\1»', text)
    return multi_replace(result, {'“': '«', '”': '»'})


def edit(text: str) -> str:
    f = compose(apply_trivial_fixes, apply_automatic_edits,
                convert_quotation_marks)
    return f(text)
